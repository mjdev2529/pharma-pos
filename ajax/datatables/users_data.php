<?php
	include '../../core/config.php';
	$data = mysqli_query($conn,"SELECT user_id, name, username, role FROM tbl_users WHERE username != 'root'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["user_id"] = $row["user_id"];
		$list["name"] = $row["name"];
		$list["username"] = $row["username"];
		$list["role"] = $row["role"] == 0?"Administrator":"Staff";
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>