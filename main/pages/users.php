<div class="main">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><span class="text-muted">Users</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2">
     <div class="col-12">
      <div class="btn-group mb-3 float-right">
        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_user">Add</button>
        <button class="btn btn-sm btn-outline-danger" onclick="delete_product()">Delete</button>
      </div>
      <div class="table-responsive">
        <table id="tbl_users" class="table table-striped table-bordered table-sm">
          <thead>
            <tr>
              <th width="15"><input type="checkbox" id="checkUser" onclick="checkAll()"></th>
              <th width="15">#</th>
              <th>Name</th>
              <th>Username</th>
              <th>Role</th>
              <th width="100">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_user" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Add new User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add_user_form">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Name</label>
              <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Username</label>
              <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Role</label>
              <select name="role" class="form-control" >
                <option value="0">Admin</option>
                <option value="1">Staff</option>
              </select>
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit_user" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Edit User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_user_form" method="post" action="#">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Name</label>
              <input type="text" name="name" id="name" class="form-control" placeholder="Name">
              <input type="hidden" name="user_id" id="user_id">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Username</label>
              <input type="text" name="username" id="username" class="form-control" placeholder="Username">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Role</label>
              <select name="role" id="role" class="form-control" >
                <option value="0">Admin</option>
                <option value="1">Staff</option>
              </select>
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    get_users();
  });

  function checkAll(){
    var x = $("#checkUser").is(":checked");

    if(x){
      $("input[name=cb_user]").prop("checked", true);
    }else{
      $("input[name=cb_user]").prop("checked", false);
    }
  }

  function get_users(){
    $("#tbl_users").DataTable().destroy();
    $("#tbl_users").dataTable({
      "ajax": {
        "type": "POST",
        "url": "../ajax/datatables/users_data.php",
      },
      "processing": true,
      "columns": [
      {
        "mRender": function(data, type, row){
          return "<input type='checkbox' value='"+row.user_id+"' name='cb_user'>";
        }
      },
      {
        "data": "count"
      },
      {
        "data": "name"
      },
      {
        "data": "username"
      },
      {
        "data": "role"
      },
      {
        "mRender": function(data, type, row){
          return "<button class='btn btn-sm btn-outline-dark' onclick='edit_user("+row.user_id+")'>Edit User</button>";
        }
      }
      ]

    });
  }

  $("#add_user_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/user_add.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
        if(data == 1){
          alert("Success! New user was added.");
          $("#add_user").modal("hide");
          $("input").val("");
          $("select").val("");
          get_users();
        }else{
          alert("Error: "+data);
        }
      }
    });
  });

  function edit_user(user_id){
    var url = "../ajax/user_details.php";
    $.ajax({
      type: "POST",
      url: url,
      data: {user_id: user_id},
      success: function(data){
        $("#edit_user").modal();
        var o = JSON.parse(data);
        $("#name").val(o.name);
        $("#username").val(o.username);
        $("#user_id").val(user_id);
        $("#role").val(o.role);
      }
    });
  }

  $("#edit_user_form").submit( function(e){
    e.preventDefault();

    var data = $(this).serialize();
    var url = "../ajax/user_edit.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
        if(data == 1){
          alert("Success! user was updated.");
          $("#edit_user").modal("hide");
          $("input").val("");
          $("select").val("");
          get_users();
        }else{
          alert("Error: "+data);
        }
      }
    });
  });

  function delete_product(){
    var conf = confirm("Are you sure to delete selected?");
    if(conf){
      var u_id = [];

      $("input[name=cb_user]:checked").each( function(){
        u_id.push($(this).val());
      });

      if(u_id.length != 0){

        var url = "../ajax/user_delete.php";

        $.ajax({
          type: "POST",
          url: url,
          data: {u_id: u_id},
          success: function(data){
            if(data != 0){
              alert("Success! Selected User/s was deleted.");
              get_users();
            }else{
              alert("Error: "+data);
            }
          }
        });
      }else{
        alert("Warning! No data selected.");
      }
    }
  }

</script>