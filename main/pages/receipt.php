<?php
  $sales_id = isset($_GET['sales_id'])?$_GET["sales_id"]:0;
  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_sales_order WHERE sales_order_id = '$sales_id'"));
?>
<div class="main">

  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <?php
      if($_GET["pf"] != "sales_report"){
    ?>
      <h1 class="h2"><a href="index.php?page=<?=page_url('sales')?>">Sales</a> / <span class="text-muted">Print Receipt</span></h1>
    <?php
      }else{
    ?>
      <h1 class="h2"><a href="index.php?page=<?=page_url('sales_report')?>">Sales Report</a> / <span class="text-muted">Print Receipt</span></h1>
    <?php
      }
    ?>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2">
    <?php
      if($_GET["pf"] != "sales_report"){
    ?>
      <div class="col-12">
        <a href="index.php?page=<?=page_url('sales')?>" class="btn btn-outline-primary mb-3"><i class="fa fa-arrow-left"></i> Back</a>
      </div>
    <?php
      }else{
    ?>
      <div class="col-12">
        <a href="index.php?page=<?=page_url('sales_report')?>" class="btn btn-outline-primary mb-3"><i class="fa fa-arrow-left"></i> Back</a>
      </div>
    <?php
      }
    ?>
    <div class="col-12">
      <div class="container">
        <div class="btn-group col-2 offset-9 mb-3">
          <button type="button" class="btn btn-outline-success" onclick="print_receipt()"><i class="fa fa-print"></i> Print</button>
        </div>
        <div id="receipt_container" class="row" style="font-family: Tahoma, sans-serif;">
            <div class="well col-10 offset-1">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <address>
                          Order Slip #: <?=$data["p_type"] == 0?$data["sales_order_id"]:$data["receipt_no"]?>
                        </address>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 text-right gprice">
                        <p>
                           <?=date("M. d, Y", strtotime($data["date_added"]))?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center gprice">
                      Order Slip
                    </div>
                    </span>
                    <table width="100%">
                  
                            <?php 
                              if($data["is_discounted"]=='1'){
                            ?>
                                     <thead>
                            <tr>
                                <td></td>
                                <td>Product</td>
                                <td >Price</td>
                                <td class="text-center">Total</td>
                            </tr>
                        </thead>
                        <tbody>
                               <?php
                                $total_qty = 0;
                                $nondiscount=0;
                                $sub_total = 0;
                                $total_vat = 0;
                                $vatable=0;
                                $vat_exempt=0;
                                $total_discount = 0;
                                $cash_tendered = $data["cash_tendered"];
                                $details_sql = mysqli_query($conn, "SELECT *, SUM(quantity)-sum(returned_quantity) as qty FROM tbl_sales_order_detail WHERE sales_order_id = '$sales_id' GROUP BY product_id");
                                while($row = mysqli_fetch_array($details_sql)){
                                $p_data = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM tbl_products WHERE product_id = '$row[product_id]'"));
                                $discount = $p_data["is_discountable"] == 1 && $data["is_discounted"] == 1?$p_data["gross_price"]*.20:0;
                                $vat = 0;
                                $vat_per_item = $vat*$row["qty"];
                                $price = $p_data["gross_price"];
                                $total_qty +=$row["qty"];
                                $total_price_sum = $row["qty"] * ($price);
                                $total_vat += $vat*$row["qty"];
                                $vatable +=$p_data["is_vatable"] == 1?$row["qty"]*$row["gross_price"]:0;
                                $vat_exempt +=$p_data["is_vatable"] == 0?$row["qty"]*$row["gross_price"]:0;
                                $discount_per_item = $discount*$row["qty"];
                                $sub_total += $total_price_sum;
                                $total_discount += $discount*$row["qty"];
                                $grand_total = $sub_total + $total_vat- $total_discount;
                                $change = $cash_tendered - $grand_total;

                                $nondiscount += $p_data["is_discountable"] == 1 && $data["is_discounted"] == 1?0:$row["qty"] * ($price);
                                ?>
                                <tr>
                                    <td> </td>
                                    <td class="prod"><small><?=$p_data["brand_name"];?></small></td>
                                    <td class="prod"><?=$row["qty"];?> @<?=number_format($price+ $vat,2)?><?= $p_data["is_vatable"] == 1? " (w/VAT)":"" ?></td>
                                  <td class="text-center"><?=number_format($total_price_sum+ $vat_per_item,2)?></td>
                                </tr>
                              <?php 
                             }?>
                             
                             <tr>
                                <td colspan="3" class="text-right">
                                <br/>
                                <!-- <p>
                                     Amt of Sale EXEMPT ITEMS:
                                  </p> -->
                                
                                  <!-- <p >
                                    Amt of Sale with VAT:
                                  </p>
                                 -->
                                  <p>
                                     Total Amount:
                                  </p>

                                  <p>
                                    Less 12 % VAT
                                  </p>

                                  <p>
                                    Price Net of VAT
                                  </p>
                              
                              
                                  <p>
                                    LESS 20% Sales Discount:
                                  </p>
                                </td>
                                <td class="text-center">
                                <br/>
                                <!-- <p>
                                   
                                  </p>
                                 
                                  <p>
                                   
                                  </p> -->
                                  <p>
                                    <?=number_format($vatable+$vat_exempt,2)?>
                                  </p>

                                  <p>
                                    <?=number_format($vatable-($vatable/1.12),2)?>
                                  </p>

                                  <p>
                                    <?=number_format($vatable-($vatable-($vatable/1.12))+$vat_exempt,2)?>
                                  </p>
                               
                                  <p>
                                    <?=number_format((($vatable-($vatable-($vatable/1.12))+$vat_exempt)- $nondiscount)*0.2,2)?>
                                  </p>
                                </td>
                            </tr>
                            <br/>
                            <tr>
                                <td colspan="3" class="text-right">Total Amount:</td>
                                <td class="text-center text-danger"><u><?=number_format(($vatable-($vatable-($vatable/1.12))+$vat_exempt)-(($vatable-($vatable-($vatable/1.12))+$vat_exempt)-$nondiscount)*0.2,2)?></u></strong></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right">
                                <br/>
                                    <p>
                                       AMOUNT TENDERED CASH :
                                    </p>  <br/>
                                    <p>
                                        Change:
                                    </p>
                                </td>
                                <td class="text-center">
                                <br/>
                                  <p>
                                    <?=number_format($cash_tendered,2)?>
                                  </p>  <br/>
                                  <p>
                                    <?=number_format($cash_tendered-(($vatable-($vatable-($vatable/1.12))+$vat_exempt)-($vatable-($vatable-($vatable/1.12))+$vat_exempt)*0.2),2)?>
                                  </p>
                                </td>
                            </tr>
                      </tbody>
                             <?php
                            
                            
                            
                            }else{

                              ?>
                  <thead>
                     <tr>
                         <td></td>
                         <td>Product</td>
                         <td class="text-right"><small>Net Price</small></td>
                         <td class="text-right">Total</td>
                     </tr>
                 </thead>
                 <tbody>
                  <?php
                    $total_qty = 0;
                    $total_with_vat_items=0;
                    $sub_total = 0;
                    $total_vat = 0;
                    $vatable=0;
                    $vat_exempt=0;
                    $total_discount = 0;
                    $vat_per_item=0;
                    $nondiscount = 0;
                    $cash_tendered = $data["cash_tendered"];
                    $details_sql = mysqli_query($conn, "SELECT *, SUM(quantity)-sum(returned_quantity) as qty FROM tbl_sales_order_detail WHERE sales_order_id = '$sales_id' GROUP BY product_id");
                    while($row = mysqli_fetch_array($details_sql)){
                    $sprice = isset($row["selling_price"])?$row["selling_price"]:0;
                    $p_data = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM tbl_products WHERE product_id = '$row[product_id]'"));

                    if($p_data){
                      $bname = $p_data["brand_name"];
                      $discount = $p_data["is_discountable"] == '1' && $data["is_discounted"] == '1'?$sprice*.20:0;
                      $vat = $p_data["is_vatable"] == 1?$sprice*.12:0;
                      $vat_per_item = $vat*$row["qty"];
                      $price = $sprice;
                      $gprice = $row["gross_price"];
                      $total_qty +=$row["qty"];
                      $total_price_sum = $row["qty"] * ($price - $discount);
                      $total_vat += $vat*$row["qty"];
                      $vatable +=$p_data["is_vatable"] == 1?($row["qty"]*$sprice)/1.12:0;
                      $total_with_vat_items +=$p_data["is_vatable"] == 1?($row["qty"]*$sprice):0;
                      $vat_exempt +=$p_data["is_vatable"] == 0?$row["qty"]*$sprice:0;

                      $sub_total += $total_price_sum;
                      $total_discount += $discount;
                      $grand_total = $sub_total ;
                      $change = $cash_tendered - $grand_total;

                      $nondiscount += $p_data["is_discountable"] == 1 && $data["is_discounted"] == 1?0:$row["qty"] * ($price);
                    }else{
                      $bname = "Item Removed";
                      $is_discountable = isset($p_data["is_discountable"])?$p_data["is_discountable"]:0;
                      $is_vatable = isset($p_data["is_vatable"])?$p_data["is_vatable"]:0;
                      $discount = $is_discountable == 1 && $data["is_discounted"] == 1?$sprice*.20:0;
                      $vat = $is_vatable == 1?$sprice*.12:0;
                      $vat_per_item = $vat*$row["qty"];
                      $price = $sprice;
                      $gprice = $row["gross_price"];
                      $total_qty +=$row["qty"];
                      $total_price_sum = $row["qty"] * ($price - $discount);
                      $total_vat += $vat*$row["qty"];
                      $vatable += $is_vatable == 1?($row["qty"]*$sprice)/1.12:0;
                      $total_with_vat_items += $is_vatable == 1?($row["qty"]*$sprice):0;
                      $vat_exempt += $is_vatable == 0?$row["qty"]*$sprice:0;

                      $sub_total += $total_price_sum;
                      $total_discount += $discount;
                      $grand_total = $sub_total ;
                      $change = $cash_tendered - $grand_total;

                      $nondiscount += $p_data["is_discountable"] == 1 && $data["is_discounted"] == 1?0:$row["qty"] * ($price);
                    }

                  ?>
                              <tr>
                                  <td></td>
                                  <td>
                                    <span class="mr-3 pl-2"><?=$row["qty"];?></span>@<?=number_format($gprice,2)?>
                                    <br>
                                    <span class="prod pl-2">
                                      <?=$bname;?>
                                    </span>
                                  </td>
                                  <td></td>
                                  <td></td>
                                  <!-- <td class="text-right gprice pt-5"><span class="mr-3"><?=number_format($price-$discount,2)?></span></td> -->
                                  <!-- <td class="text-right gprice pt-5">
                                    <?=number_format($total_price_sum,2)?>
                                  </td> -->
                              </tr>
                              <tr>
                                <td></td>
                                <td></td>
                                <td class="text-right gprice pt-3"><span class="mr-3"><?=number_format($price-$discount,2)?></span></td>
                                <td class="text-right gprice pt-3">
                                  <?=number_format($total_price_sum,2)?>
                                </td>
                              </tr>
                                  <?php


                              }
                            ?>
                            <tr>
                              <td></td>
                              <td></td>
                              <td class="text-right prod pt-2">Total Amount:</td>
                              <td class="text-right text-danger gprice pt-2"><u><?=number_format($grand_total,2)?></u></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right prod"> <br/>
                                    <p>
                                      Amount Tendered:
                                    </p>
                                    <p>
                                      Change:
                                    </p>
                                </td>
                                <td class="text-right gprice"> <br/>
                                  <p class="pl-3">
                                    <?=number_format($cash_tendered,2)?>
                                  </p>
                                  <p>
                                    <?=number_format($change,2)?>
                                  </p>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" class="text-right prod">
                                <br/>
                                  <p >
                                     VATable:
                                  </p>
                                  <p>
                                     VAT-Exempt:
                                  </p>
                                  <p>
                                    VAT:
                                  </p>
                                  <p>
                                    Amount Due:
                                  </p>
                                 
                                </td>
                                <td class="text-right gprice">
                                <br/>
                                  <p>
                                    <?=number_format($vatable,2)?>
                                  </p>
                                  <p>
                                    <?=number_format($vat_exempt,2)?>
                                  </p>
                                  <p>
                                    <?=number_format($total_with_vat_items-$vatable,2)?>
                                  </p>
                                  <p>
                                    <?=number_format(($vatable+$vat_exempt)+($total_with_vat_items-$vatable),2)?>
                                  </p>
                                
                                </td>
                            </tr>
                           
                          
                            <?php
                            
                            }
                            ?>
                          
                     
                        
                        </tbody>
                    </table>
                </div>
                <div class="col-12 text-center prod">Please ask for an Official Receipt. Thank you!</div>
            </div>
        </div>
    </div>
  </div>

</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    
  });
  function print_receipt(){
    var mywindow = window.open('', 'PRINT');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"><link rel="stylesheet" href="../assets/icons/css/all.min.css"><style type="text/css"> @media print { body, html, #receipt_container { margin: 0; height: 297mm; width: 76mm;} #receipt_container, table, h5, h1, address { font-size: xxx-large; font-family: Tahoma, sans-serif;} em, .prod { font-size: xx-large; } .gprice { font-size: xxx-large; /*font-weight: bold;*/} }</style></head><body >');
    mywindow.document.write(document.getElementById("receipt_container").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout( function(){
      mywindow.print();
      mywindow.close();
    },200);

    return true;
  }

</script>