<div class="main">

  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"> <span class="text-muted">Sales Report</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-12 mb-3">
      <form class="row" id="form_generate">
        <div class="col-2 offset-1">
          <div class="col-1 text-right h5 p-0">From: </div>
          <input type="date" class="form-control" name="from_date" value="<?=date('Y-m-d')?>">
        </div>
        <div class="col-2">
          <div class="col-1 text-right h5 p-0">To: </div>
          <input type="date" class="form-control" name="to_date" value="<?=date('Y-m-d')?>"></div>
        <div class="col-2">
          <div class="col-12 h5 p-0"><small><b>Payment Type: </b></small></div>
          <select class="form-control" name="p_type">
            <option value="-1">All</option>
            <option value="0">Cash</option>
            <option value="1">Charge</option>
          </select>
        </div>
        <div class="col-2">
          <div class="col-12 h5 p-0"><small><b>Encoder: </b></small></div>
          <select class="form-control" name="encoder">
            <option value="0">All</option>
            <?php
              $user_sql = mysqli_query($conn, "SELECT * FROM tbl_users WHERE username != 'root'");
              while($row = mysqli_fetch_array($user_sql)){
            ?>
              <option value="<?=$row['user_id']?>"><?=$row["name"]?></option>
            <?php    
              }
            ?>
          </select>
        </div>
        <div class="col-2 pt-4"><button type="submit" class="btn btn-primary mt-2"><i class="fa fa-sync-alt"></i> Generate</button></div>
      </form>
      <hr>
    </div>

    <div class="col-12 report-container">
      <h3 class="col-6 offset-3">Sales Report from <span id="from-date"><?=date('Y-m-d')?></span> to <span id="to-date"><?=date('Y-m-d')?></span></h3>
      <div class="table-responsive">
        <table id="tbl_sales_report" class="table table-striped table-bordered table-sm text-center">
          <thead>
            <tr>
              <th width="10"></th>
              <th scope="col">Receipt #</th>
              <th scope="col">Transaction Date</th>
              <th scope="col">Customer Name</th>
              <th scope="col">Payment Type</th>
              <th scope="col">Encoder</th>
              <th scope="col">Quantity</th>
              <th scope="col">Amount</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot class="bg-light">
            <tr>
              <th></th>
              <th style="border-left: none !important;"></th>
              <th style="border-left: none !important;"></th>
              <th style="border-left: none !important;"></th>
              <th style="border-left: none !important;"></th>
              <th class="text-left">TOTAL:</th>
              <th id="total_qty"></th>
              <th id="total_amt"></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

  </div>

</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    sales_report("<?=date('Y-m-d')?>","<?=date('Y-m-d')?>","-1","0");
  });

  function sales_report(fromDate,toDate,p_type, encoder){
    $("#tbl_sales_report").DataTable().destroy();
    $("#tbl_sales_report").DataTable({
      "ajax": {
        "type": "POST",
        "url": "../ajax/datatables/sales_report_data.php",
        "data":{fromDate: fromDate, toDate: toDate, p_type: p_type, encoder: encoder}
      },
      "processing": true,
      // "paging": false,
      "columns": [
      {
        "mRender": function(data, type, row){
          return "<button class='btn btn-sm btn-success' onclick='print_sales("+row.sales_id+")'><i class='fas fa-print'></i></button>";
        }
      },
      {
        "data": "receipt_no"
      },
      {
        "data": "trans_date"
      },
      {
        "data": "customer"
      },
      {
        "data": "p_type"
      },
      {
        "data": "encoded_by"
      },
      {
        "data": "quantity"
      },
      {
        "data": "amount"
      }
      ],
      "createdRow": function( row, data, dataIndex) {
        if(data.total_quantity && data.amount){
          $("#total_qty").html(data.total_quantity);
          $("#total_amt").html(data.total_amount);
          $("#from-date").html(data.fromDate);
          $("#to-date").html(data.toDate);
        }
      },
      "initComplete": function( settings, json ) {
        var api = this.api();
        if(api.rows().count() == 0){
          $("#total_qty").html(0);
          $("#total_amt").html("0.00");

          var from = $("input[name=from_date]").val();
          var to = $("input[name=to_date]").val();
          $("#from-date").html(from);
          $("#to-date").html(to);
        }
      },
      dom: 'Bfrtip',
      buttons: [
        { extend: 'print',className: 'btn btn-primary', footer: true,
          exportOptions: {
            columns: [ 1,2,3,4,5,6,7]
          }
        },
          'colvis'
      ]

    });
  }

  $("#form_generate").submit( function(e){
    e.preventDefault();
    var from = $("input[name=from_date]").val();
    var to = $("input[name=to_date]").val();
    var p_type = $("select[name=p_type]").val();
    var encoder = $("select[name=encoder]").val();
    sales_report(from,to,p_type,encoder);
  });

  function print_sales(sales_id){
    window.open("index.php?page=<?=page_url('receipt')?>&sales_id="+sales_id+"&pf=sales_report", '_blank');
  }

</script>