<div class="main">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><span class="text-muted">Purchase Return</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2">
     <div class="col-12">
      <div class="btn-group mb-3 float-right">
        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_stock_return">Add</button>
        <button class="btn btn-sm btn-outline-danger" onclick="delete_stock_return()">Delete</button>
      </div>
      <div class="table-responsive">
        <table id="tbl_stocks" class="table table-striped table-bordered table-sm">
          <thead>
            <tr>
            <th width="15"><input type="checkbox" id="checkStock" onclick="checkAll()"></th>
              <th width="15">#</th>
              <th>Product</th>
              <th>Supplier</th>
              <th width="100">Returned Quantity</th>
              <th width="100">Unit Cost</th>
              <th width="100">LOT No</th>
              <th width="100">Expiry Date</th>
              <th width="100">Date Entry</th>
         
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_stock_return" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Stock Return</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add_stocks_return">
          <div class="row">
          <div  class="col-8 offset-2 mb-3">
              <Stock>Product</label>
               <select class="custom-select d-flex" name="stock_id" id ="stock_id" style="width:100%;">
               <option value="0">Select Stock:</option>
                   <?php 
                      $products = mysqli_query($conn,"SELECT * FROM `tbl_stocks` a INNER JOIN tbl_products b on a.product_id=b.product_id where quantity !=a.sold_quantity");
                      while($row = mysqli_fetch_array($products)){
                    ?>
                      <option 
                    value="<?php echo $row['stock_id'];?>"><?php echo $row['brand_name'].", ".$row['generic_name']." - ".$row['lot_no'];?></option>
                    <?php } ?>
              </select>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Quantity</label>
              <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Quantity">
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    get_stock_return();
    $('.custom-select').select2();
  });

  function checkAll(){
    var x = $("#checkStock").is(":checked");

    if(x){
      $("input[name=cb_stock]").prop("checked", true);
    }else{
      $("input[name=cb_stock]").prop("checked", false);
    }
  }

  function get_stock_return(){
    notice_qty();
    $("#tbl_stocks").DataTable().destroy();
    $("#tbl_stocks").dataTable({
      "ajax": {
        "type": "POST",
        "url": "../ajax/datatables/stock_return_data.php",
      },
      "processing": true,
      "columns": [
      {
        "mRender": function(data, type, row){
          return "<input type='checkbox' value='"+row.stock_id+"' name='cb_stock'>";
        }
      },
      {
        "data": "count"
      },
      {
        "data": "product"
      },
      {
        "data": "supplier_name"
      },
      {
        "data": "remaining_qty"
      },
      {
        "data": "cost_price"
      }, {
        "data": "lot_no"
      },
      {
        "data": "expiry_date"
      },
      {
        "data": "date_added"
      },
      
      ]

    });
  }

  $("#add_stocks_return").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/stocks_return_add.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
       
        if(data == 1){
          alert("Success! Returned stocks added.");
          $("#add_stock_return").modal("hide");
          $("input").val("");
              $("textarea").html("");
              $("select").val(0).trigger('change');
          get_stock_return();
        }else{
          alert("Warning, Please check stocks quantity");
          $("#add_stock_return").modal("hide");
          $("input").val("");
              $("textarea").html("");
              $("select").val(0).trigger('change');
          get_stock_return();
        }
      }
    });
  });

  function delete_stock_return(){
    var conf = confirm("Are you sure to delete selected?");
    if(conf){
      var stock_id = [];

      $("input[name=cb_stock]:checked").each( function(){
        stock_id.push($(this).val());
      });

      if(stock_id.length != 0){

        var url = "../ajax/stocks_return_delete.php";

        $.ajax({
          type: "POST",
          url: url,
          data: {stock_id: stock_id},
          success: function(data){

            if(data != 0){
              alert("Success! Stocks return was deleted.");
              get_stock_return();
            }else{
              alert("Error: "+data);
            }
          }
        });
      }else{
        alert("Warning! No data selected.");
      }
    }
  }

</script>