<?php
  if($page == page_url('dashboard')){
    $dashboard = "active";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('sales')){
    $dashboard = "";
    $sales = "active";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('products')){
    $dashboard = "";
    $sales = "";
    $products = "active";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('suppliers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "active";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('stocks')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "active";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('customers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "active";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('sales_report')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "active";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "";
  }
  else if($page == page_url('inventory_report')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "active";
    $p_return = "";
    $s_return = "";
    $users = "";
  }else if($page == page_url('p_return')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "active";
    $s_return = "";
    $users = "";
  }else if($page == page_url('s_return')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "active";
    $users = "";
  }else if($page == page_url('users')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $users = "active";
  }
?>
 <div class="sidebar-sticky pt-3">
  	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>navigation</span>
    </h6>
    <ul class="nav flex-column">
      <!-- <li class="nav-item">
        <a class="nav-link h6 <?=$dashboard?>" href="index.php?page=<?=page_url('dashboard')?>">
          <span class="fa fa-home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link h6 <?=$sales?>" href="index.php?page=<?=page_url('sales')?>">
          <span class="fa fa-shopping-cart"></span>
          Sales <span class="sr-only">(current)</span>
        </a>
      </li>

        <li class="nav-item">
          <a class="nav-link h6 <?=$p_return?>" href="index.php?page=<?=page_url('p_return')?>">
            <span class="fa fa-undo"></span>
            Sales Return
          </a>
        </li>

        
        <li class="nav-item">
          <a class="nav-link h6 <?=$customers?>" href="index.php?page=<?=page_url('customers')?>">
            <span class="fa fa-users"></span>
            Customers
          </a>
        </li>

      <?php if($_SESSION["role"] == 0){?>

        <li class="nav-item">
          <a class="nav-link h6 <?=$users?>" href="index.php?page=<?=page_url('users')?>">
            <span class="fa fa-user-plus"></span>
            Users
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$products?>" href="index.php?page=<?=page_url('products')?>">
            <span class="fa fa-cubes"></span>
            Products
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$stocks?>" href="index.php?page=<?=page_url('stocks')?>">
              <span class="fa fa-file-alt"></span>
              Stocks
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$suppliers?>" href="index.php?page=<?=page_url('suppliers')?>">
            <span class="fa fa-truck-moving"></span>
            Suppliers
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$s_return?>" href="index.php?page=<?=page_url('s_return')?>">
            <span class="fa fa-boxes"></span>
            Purchase Return
          </a>
        </li>
      <?php } ?>
    </ul>

    <?php if($_SESSION["role"] == 0 || $_SESSION["role"] == 1){?>
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>reports</span>
      </h6>
      <ul class="nav flex-column mb-2">
        <li class="nav-item">
         <a class="nav-link h6 <?=$sales_report?>" href="index.php?page=<?=page_url('sales_report')?>">
            <span class="fa fa-chart-bar"></span>
            Sales Report
          </a>
        </li>
        <li class="nav-item">
         <a class="nav-link h6 <?=$inv_report?>" href="index.php?page=<?=page_url('inventory_report')?>">
            <span class="fa fa-archive"></span>
            Inventory Report
            <span class="badge badge-warning float-right notice-pill"></span>
          </a>
        </li>
      </ul>
    <?php } ?>
</div>