-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2022 at 01:35 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u773984407_pharma_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_address` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`customer_id`, `customer_name`, `customer_address`) VALUES
(1, 'Walk In', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(11) NOT NULL,
  `brand_name` varchar(255) DEFAULT '""',
  `generic_name` varchar(255) DEFAULT '""',
  `category_description` varchar(255) DEFAULT '""',
  `price` decimal(13,2) DEFAULT 0.00,
  `gross_price` decimal(13,2) DEFAULT 0.00,
  `is_vatable` int(11) DEFAULT 0,
  `is_discountable` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `brand_name`, `generic_name`, `category_description`, `price`, `gross_price`, `is_vatable`, `is_discountable`) VALUES
(1, 'Biogesic', 'Paracetamol', '', '4.00', '5.00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_order`
--

CREATE TABLE `tbl_sales_order` (
  `sales_order_id` int(11) NOT NULL,
  `receipt_no` bigint(20) DEFAULT NULL,
  `customer_id` int(11) DEFAULT 0,
  `status` varchar(255) DEFAULT '0',
  `date_updated` datetime DEFAULT current_timestamp(),
  `date_added` datetime DEFAULT current_timestamp(),
  `user_id` int(11) DEFAULT 0,
  `cash_tendered` decimal(12,2) DEFAULT 0.00,
  `is_discounted` int(11) DEFAULT 0,
  `p_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_order_detail`
--

CREATE TABLE `tbl_sales_order_detail` (
  `sales_order_detail_id` int(11) NOT NULL,
  `sales_order_id` int(11) DEFAULT 0,
  `product_id` int(11) DEFAULT 0,
  `stock_id` int(11) DEFAULT 0,
  `quantity` int(11) DEFAULT 0,
  `returned_quantity` int(11) DEFAULT 0,
  `selling_price` decimal(13,2) DEFAULT 0.00,
  `gross_price` decimal(13,2) DEFAULT 0.00,
  `date_added` datetime DEFAULT current_timestamp(),
  `date_updated` datetime DEFAULT current_timestamp(),
  `status` varchar(10) DEFAULT '""',
  `discount` float(255,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stocks`
--

CREATE TABLE `tbl_stocks` (
  `stock_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT 0,
  `supplier_id` int(11) DEFAULT 0,
  `cost_price` decimal(12,2) DEFAULT 0.00,
  `quantity` int(11) DEFAULT 0,
  `sold_quantity` int(11) DEFAULT 0,
  `returned_quantity` int(11) DEFAULT 0,
  `lot_no` varchar(255) DEFAULT '""',
  `expiry_date` date DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_returned` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_stocks`
--

INSERT INTO `tbl_stocks` (`stock_id`, `product_id`, `supplier_id`, `cost_price`, `quantity`, `sold_quantity`, `returned_quantity`, `lot_no`, `expiry_date`, `date_added`, `date_updated`, `date_returned`) VALUES
(1, 1, 1, '0.00', 500, 0, 0, '123', '2024-02-21', '2022-02-21 00:00:00', '2022-02-21 00:00:00', '2022-02-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `supplier_name`) VALUES
(1, 'Test Supplier');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `password`, `role`) VALUES
(1, 'Admin User', 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 0),
(2, 'Root Account', 'root', '827ccb0eea8a706c4c34a16891f84e7b', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_sales_order`
--
ALTER TABLE `tbl_sales_order`
  ADD PRIMARY KEY (`sales_order_id`);

--
-- Indexes for table `tbl_sales_order_detail`
--
ALTER TABLE `tbl_sales_order_detail`
  ADD PRIMARY KEY (`sales_order_detail_id`),
  ADD KEY `sales_order_id` (`sales_order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `date_added` (`date_added`),
  ADD KEY `quantity` (`quantity`),
  ADD KEY `returned_quantity` (`returned_quantity`);

--
-- Indexes for table `tbl_stocks`
--
ALTER TABLE `tbl_stocks`
  ADD PRIMARY KEY (`stock_id`),
  ADD KEY `quantity` (`quantity`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `date_added` (`date_added`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD UNIQUE KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_sales_order`
--
ALTER TABLE `tbl_sales_order`
  MODIFY `sales_order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_sales_order_detail`
--
ALTER TABLE `tbl_sales_order_detail`
  MODIFY `sales_order_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_stocks`
--
ALTER TABLE `tbl_stocks`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
